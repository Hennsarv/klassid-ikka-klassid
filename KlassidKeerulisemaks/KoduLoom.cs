﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidKeerulisemaks
{
    class KoduLoom : Loom
    {
        // väljad tuletatud klassis lisanduvad baasklassi omadele
        public string Tõug; // tal on nii Liik kui Tõug

        public KoduLoom(string liik, string tõug = "tundmatu") : base(liik)
        {
            Tõug = tõug;

        }

        public override void TeeHäält()
        {
            Console.WriteLine($"Koduloom {Liik} teeb mahedat häält");
        }

        public override string ToString()
        {
            return $"Meil on koduloom {Liik} {Tõug} tõugu";
        }

    }

}
