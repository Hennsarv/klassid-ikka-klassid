﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidKeerulisemaks
{

    

    class Kass : KoduLoom, ISöödav, IComparable    
    {
        public string Nimi;
        bool Tuju = false;
 
        public Kass(string nimi = "nimeta", string tõug = "tundmatu") : base("kass", tõug)
        {
            Nimi = nimi;
        }

        public void Silita()
        {
            Tuju = true;
        }
        public void Sikuta()
        {
            Tuju = false;
        }

        public override string ToString()
        {
            return $"meil on kiisu {Nimi} kes on {Tõug} tõugu";
        }

        public override void TeeHäält()
        {
            if (Tuju)
                Console.WriteLine($"Kass {Nimi} lööb nurru");
            else
                Console.WriteLine($"Kass {Nimi} kräunub");
        }

        public void Süüakse()
        {
            Console.WriteLine($"Kass {Nimi} pannakse nahka");
        }

        public int CompareTo(object obj)
        {
            return (obj is Kass) ?  Nimi.CompareTo(((Kass)obj).Nimi) : -1;
        }
    }


}
