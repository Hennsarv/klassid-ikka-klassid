﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidKeerulisemaks
{

  

    abstract class Elajas
    {

        public abstract void LiikumisViis();

        //public void Süüakse()
        //{
        //    Console.WriteLine($"{this} pistetakse pintslisse");
        //}

    }

    class Lind : Elajas
    {
        public override void LiikumisViis()
        {
            Console.WriteLine("Lind lendab") ;
        }
    }

    class Loom : Elajas
    {
        public static List<Loom> Loomad = new List<Loom>();
        // väljad baasklassis
        public string Liik;

        //        public Loom() : this("tundmatu")
        //       { }

        public Loom(string liik = "tundmatu")
        {
            Liik = liik;
            Loomad.Add(this);
        }

        public override void LiikumisViis()
        {
            Console.WriteLine($"{this} jookseb");
        }

        public virtual void TeeHäält() // tohib tuletatud klassis ära vahetada
        {
            Console.WriteLine($"Loom liigist {Liik} teeb hirmsat häält");
        }

        public override string ToString()
        {
            return $"meil on loom {Liik}";
        }

    }

}
