﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidKeerulisemaks
{
    interface ISöödav
    {
        void Süüakse();
    }

    class Sepik : ISöödav
    {
        void ISöödav.Süüakse()
        {
            Console.WriteLine("Keegi nosib sepikut");
        }
    }
}
