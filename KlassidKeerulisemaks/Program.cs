﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KlassidKeerulisemaks
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom l1 = new Loom("krokodill");
            l1.TeeHäält();

 
            Loom k = new KoduLoom("lehm", "hereford");
            k.TeeHäält();

           

            Kass miisu = new Kass("Miisu", "siiami");
            miisu.TeeHäält();
            miisu.Silita();
            miisu.TeeHäält();
            //miisu.Sikuta();
            miisu.TeeHäält();

            miisu.Süüakse();

            int loendur = 0;
            foreach (var x in Loom.Loomad)
            {
                x.TeeHäält();
                Console.WriteLine($"loom numbriga {++loendur} on {x}");
            }

            List<Elajas> elajad = new List<Elajas>();
            elajad.Add(new Lind());
            elajad.Add(new Lind());
            elajad.Add(new Lind());
            foreach (var x in Loom.Loomad) elajad.Add(x);

            foreach (var e in elajad)
                e.LiikumisViis();


            SortedSet<Kass> kassid = new SortedSet<Kass>
            {
                new Kass("Tihane"),
                new Kass("Mummel"),
                new Kass("Triibuliine")
            };

            foreach (var x in kassid) Console.WriteLine(x);

            Lõuna(kassid.ToList()[0]);

            Lõuna(new Sepik());

            Lõuna(l1); // krokodilli ei saa lõunale kaasa võtta


        }

        class MinuList<T> : List<T>
        {
            public void Trüki()
            {
                foreach (var x in this) Console.WriteLine(x);
            }
        }


        public static void Lõuna(object o)
        {

            Console.WriteLine("ma nüüd lõunatan");

            // lõunale kaasaantud asi ei pruugi olla ISöödav

            // var 1 - kontollime ja castime (ISöödav)
            if (o is ISöödav)
                ((ISöödav)o).Süüakse();

            // var 2 - omoistame as ja kontrollime tulemust
            ISöödav öö = o as ISöödav;
            if (öö != null) öö.Süüakse();

            // var 3 - kasutame ?. tehet
            //          vasakul on null, siis meetodit ei käivitata
            (o as ISöödav)?.Süüakse();


            Console.WriteLine("laseme nüüd leiba luusse" );
        }


    }

   
   
 

}
