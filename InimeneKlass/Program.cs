﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeneKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            #region proovimiseks
            //Console.WriteLine((new Inimene("41012030000", "hennu ema").Vanus0));
            //Console.WriteLine((new Inimene("35503070211", "henn").Vanus0));
            //Console.WriteLine((new Inimene("60104180000", "tütarlaps").Vanus2));
            //Console.WriteLine((new Inimene("60004180000", "tütarlaps2").Vanus2));


            //Console.WriteLine((int)'0');

            //Console.WriteLine("35503070211"[0] - '1');

            #endregion

            string filename = @"..\..\Nimekiri.txt";
            string[] loetudRead = File.ReadAllLines(filename);
            List<Inimene> inimesed = new List<Inimene>();
            foreach (string loetudRida in loetudRead)
            {
                string[] osad = (loetudRida + "  ").Split(' ');
                inimesed.Add(new Inimene(osad[0], osad[1], osad[2]));
            }

            int loendur = 0;
            foreach (var x in inimesed)
            {
                Console.WriteLine($"{++loendur}. {x}");
            }

        }
    }
}
