﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeneKlass
{
    public class Inimene
    {
        public readonly string Isikukood; // ainult konstruktoris saab omistada
        string _Eesnimi;
        string _Perenimi;

        //public Inimene(string isikukood)
        //{
        //    _Isikukood = isikukood;
        //}

        public Inimene(string isikukood, string eesnimi = "", string perenimi= "")
        {
            Isikukood = isikukood;
            Eesnimi = eesnimi;
            Perenimi = perenimi;
        }

        public string Eesnimi
        {
            get => _Eesnimi;
            set { _Eesnimi = Suurtäht(value); }
        }
        public string Perenimi
        {
            get => _Perenimi;
            set { _Perenimi = Suurtäht(value); }
        }

        //public string Isikukood // read only property
        //{
        //    get { return _Isikukood; }
        //}

        public Sugu Sugu => (Sugu)(Isikukood[0] % 2);

        public DateTime Sünniaeg => new DateTime(
            ((Isikukood[0] - '1') / 2 + 18) * 100 + int.Parse(Isikukood.Substring(1,2)), // aasta
            int.Parse(Isikukood.Substring(3,2)), // kuu
            int.Parse(Isikukood.Substring(5,2))                                     // päev
            );

        public int Vanus =>
            DateTime.Now.Year - Sünniaeg.Year 
            + (DateTime.Now.DayOfYear >= Sünniaeg.DayOfYear ? 0 : -1)
            ;

        public int Vanus2 => (int)((DateTime.Now.Date - Sünniaeg).TotalDays / 365.25);

        public int Vanus0
        {
            get
            {
                int d = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(d) > DateTime.Now.Date) d--; 
                
                return d;
            }
        }

        public string Täisnimi => $"{Eesnimi} {Perenimi}";

        static string Suurtäht(string s)
            => 
            s.Length == 0 ? s : 
            s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();

        public override string ToString()
        {
            return $"{Sugu} {Eesnimi} {Perenimi} (sünd: {Sünniaeg.ToShortDateString()})";
        }

    }
}
