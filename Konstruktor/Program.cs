﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (false)
            {
                I i = I.TeeI("35503070211");
                Console.WriteLine(i?.Isikukood);
                I x = I.TeeI("loll inimene");
                Console.WriteLine(x?.Isikukood);


                Inimene i1 = Õpetaja.Create("35503070211", "Henn", "Sarv", "matemaatika");
                Õpilane õ1 = Õpilane.Create("50101010238", "Mati", "Maasikas", "1A");
                Õpilane õ2 = Õpilane.Create("45705210329", "Maris", "Sarv", "3A");
            }

            string asukoht = @"..\..\";
            string õpilasedFile = asukoht + "õpilased.txt";
            string õpetajadFile = asukoht + "õpetajad.txt";

            foreach(var rida in File.ReadAllLines(õpilasedFile))
            {
                string[] osad = (rida + "   ").Split(' ');
                Õpilane.Create(osad[0], osad[1], osad[2], osad[3]);
            }

            //foreach (var rida in File.ReadAllLines(õpetajadFile))
            //{
            //    string[] osad = (rida + "   ").Split(' ');
            //    Õpetaja.Create(osad[0], osad[1], osad[2], osad[3]);
            //}

            File.ReadAllLines(õpetajadFile)     // string[]
                .Select(x => (x+"   ").Split(' '))      // IEnumerable<string[]>
                .ToList()         // List<string[]>
                //.ForEach(x => new Õpetaja(x[0], x[1], x[2], x[3]))
                .ForEach(x => Õpetaja.Create(x[0], x[1], x[2], x[3]))
                ;





            Console.WriteLine("\nTrükime välja inimesed:");
            foreach (var e in Inimene.Inimesed)
            {
                Console.WriteLine($"\t{e}");
            }

            Console.WriteLine("\nTrükime välja ainult õpilased 1A klassis:");
            foreach 
                (var e in Inimene.Inimesed.Where(x => x is Õpilane).Where(x => ((Õpilane)x).Klass == "1A"))
            {
                
                    Console.WriteLine($"\t{e}");
                
            }
            Console.WriteLine("\nTrükime välja ainult õpetajad:");
            foreach (var e in Inimene.Inimesed)
            {
                if (e is Õpetaja)
                {
                    Console.WriteLine($"\t{e}");
                }
            }

            Console.WriteLine("2.A klasssis õpib õpilasi:");
            Console.WriteLine(
                Inimene.Inimesed
                .Where(x => x is Õpilane)
                .Where(x => ((Õpilane)x).Klass == "2A")
                .Count()
                );
            Console.WriteLine(
                Inimene.Inimesed   // torust tulevad inimesed
                .OfType<Õpilane>()  // edasi lähevad õpilased (ainult)
                .Where(x => x.Klass == "2A")
                .Count()
                );

            Console.WriteLine("Kõigi õpilaste keskmine vanus");
            Console.WriteLine(
                Inimene.Inimesed        // IEnumerable<Inimene>    torust tulevad inimesed
                .Where(x => x is Õpilane) // IEnumerable<Inimene>  edasi lähevad need inimesed, kes on õpilased
                .Select(x => x.Vanus)   // IEnumerable<int>
                .Average()              // double
                );

            Console.WriteLine(
                
                );

            Dictionary<string, Inimene> nimekiri = 
                Inimene.Inimesed
                .ToDictionary(x => x.Isikukood, x => x);

            Console.WriteLine(nimekiri["35503070211"]);


            Console.WriteLine("Inimesed tähestiku järjekorras");
            foreach(var x in Inimene.Inimesed
                .OrderBy(x => x.Täisnimi)

                ) Console.WriteLine(x);

            Console.WriteLine("Inimesed sünnipäeva järjekorras");
            foreach(var x in Inimene.Inimesed
                .OrderBy(x => x.Sünniaeg.Month)
                .ThenBy(x => x.Sünniaeg.Day)
                ) Console.WriteLine(x);

            Console.WriteLine("Inimesed vanuse järjekorras:");
            foreach(var x in Inimene.Inimesed
                .OrderBy(x => x.Sünniaeg)
                ) Console.WriteLine(x);

        }
    }

    class I
    {
        public readonly string Isikukood;
        private I(string isikukood) { Isikukood = isikukood; }

        public static I TeeI(string isikukood)

        {
            if (isikukood.Length == 11 && long.TryParse(isikukood, out long ikn))
            {
                return new I(isikukood);
            }
            else return null;


        }
    }

   
}
