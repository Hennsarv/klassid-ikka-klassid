﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktor
{
    // tegin klassi õpilane factori põhisena
    // see kontrollib, et IK oleks õige

    class Õpilane : Inimene
    {
        public string Klass; // klass kus ta õpib

        protected Õpilane(string isikukood, string eesnimi="", string perenimi="", string klass="")
            : base(isikukood, eesnimi, perenimi)
        {
            Klass = klass;
        }

        public static Õpilane Create(string isikukood, string eesnimi = "", string perenimi = "", string klass = "")
        {
            if (CheckIK(isikukood)) return new Õpilane(isikukood, eesnimi, perenimi, klass);
            else return null;
        }

        public override string ToString()
        {
            return $"{Klass} klassi õpilane " + base.ToString();
        }
    }
}
