﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktor
{
    // tegin klassi Inimene factory põhisena
    // klassis on kinnine (protected) konstruktor 
    // ja static funktsioon Create

    public class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();

        public readonly string Isikukood; // ainult konstruktoris saab omistada
        string _Eesnimi;
        string _Perenimi;

        //public Inimene(string isikukood)
        //{
        //    _Isikukood = isikukood;
        //}

        protected Inimene(string isikukood, string eesnimi = "", string perenimi = "")
        {
            Isikukood = isikukood;
            Eesnimi = eesnimi;
            Perenimi = perenimi;
            Inimesed.Add(this);
        }

        public static Inimene Create(string isikukood, string eesnimi = "", string perenimi = "")
        {
            if (CheckIK(isikukood)) return new Inimene(isikukood, eesnimi, perenimi);
            else return null;
        }

        // see funktsioon, kontrolllib isikukoodi õigsust
        protected static bool CheckIK(string isikukood)  
            // Kui lisada järgnevale reale => ette reavahetus,
            // Lülitatakse alljärgnev funktsioon välja
            // => true; /*
        {
            int i = 0;
            int kj = isikukood
                .Substring(0, 10)
                .Select(x => x - '0')
                .ToArray()
                .Select(x => x * ((i++ % 9) + 1))
                .Sum() % 11;
            if (kj == 10)
            {
                i = 2;
                kj = isikukood
                    .Substring(0, 10)
                    .ToArray()
                    .Select(x => x * ((i++ % 9) + 1))
                    .Sum() % 11 % 10;
            }
            return 
                DateTime.TryParse(
                    $"{((isikukood[0] - '1') / 2 + 18)}{isikukood.Substring(1, 2)}/{isikukood.Substring(3, 2)}/{isikukood.Substring(5, 2)}", out DateTime dt)
                && kj == isikukood[10] - '0'  // selle rea väljakommenteerimine ei kontrolli viimast numbrit
                ;
        }
        // */
        public string Eesnimi
        {
            get => _Eesnimi;
            set { _Eesnimi = Suurtäht(value); }
        }
        public string Perenimi
        {
            get => _Perenimi;
            set { _Perenimi = Suurtäht(value); }
        }

        //public string Isikukood // read only property
        //{
        //    get { return _Isikukood; }
        //}

        public Sugu Sugu => (Sugu)(Isikukood[0] % 2);

        public DateTime Sünniaeg => new DateTime(
            ((Isikukood[0] - '1') / 2 + 18) * 100 + int.Parse(Isikukood.Substring(1, 2)), // aasta
            int.Parse(Isikukood.Substring(3, 2)), // kuu
            int.Parse(Isikukood.Substring(5, 2))                                     // päev
            );

        public int Vanus =>
            DateTime.Now.Year - Sünniaeg.Year
            + (DateTime.Now.DayOfYear >= Sünniaeg.DayOfYear ? 0 : -1)
            ;

        public int Vanus2 => (int)((DateTime.Now.Date - Sünniaeg).TotalDays / 365.25);

        public int Vanus0
        {
            get
            {
                int d = DateTime.Now.Year - Sünniaeg.Year;
                if (Sünniaeg.AddYears(d) > DateTime.Now.Date) d--;

                return d;
            }
        }

        public string Täisnimi => $"{Eesnimi} {Perenimi}";

        static string Suurtäht(string s)
            =>
            s.Length == 0 ? s :
            s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();

        public override string ToString()
        {
            return $"{Sugu} {Eesnimi} {Perenimi} (sünd: {Sünniaeg.ToShortDateString()})";
        }

    }
    public enum Sugu { Naine, Mees }
}
