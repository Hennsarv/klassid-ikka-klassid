﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konstruktor
{
    class Õpetaja : Inimene
    {
        public string Aine; // aine mida õpetab

        // see konstruktor võiks olla protected
        // siis ei saa teha ilma IK kontrollimiseta inimesi
        public Õpetaja(string isikukood, string eesnimi = "", string perenimi = "", string aine = "")
            : base(isikukood, eesnimi, perenimi)
        {
            Aine = aine;
        }

        public static Õpetaja Create(string isikukood, string eesnimi = "", string perenimi = "", string aine = "")
        {
            if (CheckIK(isikukood)) return new Õpetaja(isikukood, eesnimi, perenimi, aine);
            else return null;
        }

        public override string ToString()
        {
            return $"{Aine} õpetaja " + base.ToString();
        }
    }
}
